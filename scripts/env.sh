dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
root="$(dirname "$dir")"

export PGDATABASE="dune_ton_merge"
export PGCUSTOM_CONVERTERS_CONFIG="$root/src/db/custom_converters"
