#!/bin/bash

. ./env.sh

$FT call dune_airdrops deployBatch --sign admin --subst @%{res:addr} --output batch.addr || exit 2

$FT account remove batch

$FT account create batch --address $(cat batch.addr) --contract DuneAirdrop || exit 2

for i in `seq 1 100`; do

    echo $i
    $FT call batch addTransfer '{ "ton_amount": "%{ton:9000}", "dest_addr": "%{account:address:admin}", "dest_pubkey": "0x%{account:pubkey:admin}" }' --sign admin || exit 2

done

$FT call batch get --local || exit 2
$FT account state batch

$FT multisig transfer 1000000 --from freeton_giver --to batch || exit 2


#for i in `seq 1 3`; do
#    $FT call batch executeTransfer --sign admin || exit 2
#done

$FT call batch executeBatch --sign admin || exit 2

$FT account state batch
$FT call batch reset --sign admin || exit 2
$FT account state batch admin
