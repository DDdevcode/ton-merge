# Running Tests
=============

## Setting up

This is going to setup the set of accounts needed to test the bridge, and
the database for 3 relays.

### Sandbox network

Use the following command to setup the sandbox:
```
make init-sandbox
```

### Testnet network

You will need to prepare first a specific wallet for your test, in a network
called `testnet0`. The tests themselves will happen in a network `testnet1`,
initialized with a copy of testnet0.

You will need to create the following accounts on `ft`:
* a Surf account 'admin' with the least 40010 TON on it
* an `freeton_giver` account that will receive the remaining tokens at
    the end of the swap
* a 'depool' contract for vesting

(note that you can change the 40010 TON in env.sh to a lower value)

Then use:
```
make init-testnet
```

## Running tests

To start the 3 relays locally, use:
```
./06_start_relays.sh
```

If you don't have the Dune part, just use the following tests:

```
./07_enter_orl_swap.sh
```
(this should perform the ORL vested swap)

```
./07_enter_swaps.sh
```
(this should send 4 swap orders for users user0, user1, user2 and user3)

Use `ft account` to check the current state of all accounts. You should
have the following lines:
```
Account "depool": 199.977_098_044 TONs
Account "user3": 795.228_628_230 TONs
Account "user2": 596.421_471_172 TONs
Account "user1": 397.614_314_115 TONs
Account "user0": 198.807_157_057 TONs
```

Secret for orl is "bonjour", and "Bonjour" for user0-user3


Check all the *.log files in case of errors.


## Setting up `testnet0`

First, on `testnet`, create a new Surf account `freeton_giver`:
```
ft account --create freeton_giver --surf  
```

Keep the passphrase for later, and move at least 40000 Rubies to it:
``
ft multisig -a deployer --transfer 40000 --to freeton_giver --parrain
ft multisig -a freeton_giver --create
```

Now, let's switch to `testnet0`:
```
ft switch --create testnet0 --url https://net.ton.dev
ft switch
```

Now, take the PASSPHRASE of a Surf account on testnet holding more than
20000 TON.

```
ft account --create freeton_giver --passphrase "PASSPHRASE" --surf
ft account
```

The account command should show that you have about 40000 rubies on it.

Create also 10 user accounts, from 0 to 9:

```
for i in 0 1 2 3 4 5 6 7 8 9; do ft account --create user$i --surf; done
```

```
ft account --create admin --surf
ft multisig -a freeton_giver --transfer 1 --to admin --parrain
ft multisig -a admin --create
```

Now, we need to create a `depool` account. For that, go to
https://ton.live, and select "DePools" in the "Staking" menu. Choose
one of the active ones (with a min stake of max 100 TON).


```
ft account --create orl --surf
ft multisig -a freeton_giver --transfer 1 --to orl --parrain
ft multisig -a orl --create
```

