#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH

echo ${FREETON_RELAY} --close-swap "$TON_MERGE_PASSPHRASE"
${FREETON_RELAY} --close-swap "$TON_MERGE_PASSPHRASE"

for i in 0 1 2 3 4 5 6 7 8 9; do
    
  $FT multisig create user$i
  $FT multisig transfer all --from user$i --to freeton_giver

done
