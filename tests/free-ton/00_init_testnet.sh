#!/bin/bash

echo -n testnet1 > NETWORK

. ./env.sh

$FT switch mainnet

echo You must have testnet0 configured:
$FT switch testnet0 ||  exit 2 

cmdk ft switch --remove testnet1

$FT switch --create testnet1 --url https://net.ton.dev

FT_SWITCH=${NETWORK}
export FT_SWITCH

mkdir -p $HOME/.ft/testnet1/
cp $HOME/.ft/testnet0/wallet.json $HOME/.ft/testnet1/wallet.json

