#!/bin/bash

echo -n sandbox1 > NETWORK

. ./env.sh

$FT switch to mainnet || exit 2

FT_SWITCH=${NETWORK}

rm -f *.block *.calc

killall ft
killall ton-merge-freeton
./10_kill_relays.sh 

$FT --switch ${NETWORK} node stop
$FT switch remove ${NETWORK}
$FT switch create ${NETWORK} || exit 2

export FT_SWITCH

$FT node start || exit 2
echo
echo Waiting 10 seconds for warmup
sleep 10

export FTGIVER_PASSPHRASE="dolphin mansion aunt water put wealth crisp invite smoke nasty myth lens"

$FT account create freeton_giver --surf --passphrase "%{env:FTGIVER_PASSPHRASE}" || exit 2
$FT node give freeton_giver --amount ${ADMIN_TONS} || exit 2


export ADMIN_PASSPHRASE="race hospital smart require radar sunny trim bike receive fashion disease unable"

$FT account create admin --surf --passphrase "%{env:ADMIN_PASSPHRASE}" || exit 2
$FT multisig transfer 1 --from freeton_giver --to admin --parrain || exit 2
$FT multisig create admin || exit 2





export ORL_PASSPHRASE="raise donate end life special cargo battle smart outside pill mango mammal"

$FT account create orl --surf --passphrase "%{env:ORL_PASSPHRASE}" || exit 2
$FT multisig transfer 200 --from freeton_giver --to orl --parrain || exit 2
$FT multisig create orl || exit 2


# $FT node --give freeton_giver:95000 || exit 2

$FT account remove depool

$FT contract deploy FakeDePool --create depool --deployer freeton_giver || exit 2

$FT watch --account depool &> watch-depool.log &

