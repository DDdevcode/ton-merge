#!/bin/bash

set -euo pipefail

. ./env.sh

$FT output 'TESTUSER address: %{account:address:%{env:TESTUSER}}'
$FT output 'TESTUSER pubkey: 0x%{account:pubkey:%{env:TESTUSER}}'
$FT output 'TESTUSER passphrase: 0x%{account:passphrase:%{env:TESTUSER}}'

$FT client -- debot fetch %{account:address:DuneDebot}
