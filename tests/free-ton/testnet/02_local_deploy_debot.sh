#!/bin/bash

set -euo pipefail

. ./env.sh

# Use this script with a modified version of tonos-cli for local caching of debots

# $FT --switch sandbox1 contract deploy DuneDebot --credit 1 '{}' -f

# $FT --switch sandbox1 call DuneDebot setABI '{ "dabi": "%{hex:read:contract:abi:DuneDebot}" }'

$FT --switch sandbox1 call DuneDebot setIcon '{ "icon": "%{hex:string:data:image/png;base64,%{base64:file:DuneDebot.png}}" }'

$FT --switch sandbox1 call DuneDebot setRootContract '{ "root": "%{net-account:testnet:address:root_address}" }'

$FT --switch sandbox1 call DuneDebot setDePools '{ "names": [ "%{hex:string:Vigorous Hellman}" , "%{hex:string:Distracted Ptolemy}", "%{hex:string:Kind Black}"], "addresses": [ "%{net-account:testnet:address:depool1}", "%{net-account:testnet:address:depool2}", "%{net-account:testnet:address:depool3}" ] }'

$FT --switch sandbox1 call DuneDebot setSurfCode '{ "code": "%{get-code:contract:tvc:SetcodeMultisigWallet2}"}'

DEBOT_SAVE_DIR=$HOME/.ft/debots $FT --switch sandbox1 client -- debot fetch %{net-account:sandbox1:address:DuneDebot}

