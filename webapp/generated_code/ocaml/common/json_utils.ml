exception DecodingError of string
exception EncodingError of string

type json = Ezjsonm.value

type nat = Z.t
type dun = Z.t
type timestamp = string

let json = Json_encoding.any_ezjson_value

let string_encoding = Json_encoding.string

let unit_encoding = Json_encoding.unit

let encode_string s =
  `String s

let decode_string = function
  | `String s -> s
  | _ -> raise (DecodingError "Bad string")

let encode_pair1 encode (d1, d2) =
  `A [ encode d1; encode d2 ]

let decode_pair1 decode = function
  | `A [ d1; d2 ] -> (decode d1, decode d2)
  | _ -> raise (DecodingError "Bad pair")

let encode_pair2 encode1 encode2 (d1, d2) =
  `A [ encode1 d1; encode2 d2 ]

let decode_pair2 decode1 decode2 = function
  | `A [ d1; d2 ] -> (decode1 d1, decode2 d2)
  | _ -> raise (DecodingError "Bad pair")

let encode_pair_str_x encode (s, e) = s, encode e

let decode_pair_str_x decode (s, e) = s, decode e

let encode_tuple3 encode1 encode2 encode3 (d1, d2, d3) =
  `A [encode1 d1; encode2 d2; encode3 d3]

let decode_tuple3 decode1 decode2 decode3 = function
    `A [d1; d2; d3] -> (decode1 d1, decode2 d2, decode3 d3)
  | _ -> raise (DecodingError "Bad tuple3")

let encode_option encode = function
  | Some e -> `O [ "some", encode e ]
  | None -> `String "none"

let decode_option decode = function
  | `O [ "some", e ] -> Some (decode e)
  | `String "none" -> None
  | _ -> raise (DecodingError "Bad option")

let encode_list encode = function
  | [] -> `Null
  | l -> `A (List.map encode l)

let decode_list decode = function
  | `Null -> []
  | `A l -> (List.map decode l)
  | `O [s, _] -> raise (DecodingError ("Bad list " ^ s))
  | _ -> raise (DecodingError "Bad list")

let encode_bindings_list encode l =
  `O (List.map (fun (s, d) -> s, encode d) l)

let decode_bindings_list decode = function
  | `O l -> List.map (fun (s, d) -> s, decode d) l
  | _ -> raise (DecodingError "Bad bindings list")

let encode_str_x_list encode l = `O (List.map (encode_pair_str_x encode) l)

let decode_str_x_list decode e =
  match e with
    `O l -> (List.map (decode_pair_str_x decode) l)
  | `A _ -> decode_list (decode_pair2 decode_string decode) e
  | _ -> raise (DecodingError "Bad (string * 'a) list ")

let dune_expr enc = Json_encoding.(obj1 (req "dune_expr" enc))

let enc_or_null_encode_as_option enc =
  let open Json_encoding in
  fun x -> construct (option enc) x
let enc_or_null_decode_as_option enc = function
  | `Null -> None
  | json -> Some (Json_encoding.destruct enc json)

let enc_or_null_encoding_as_option enc =
  let open Json_encoding in
  conv
    (enc_or_null_encode_as_option enc)
    (enc_or_null_decode_as_option enc)
  json

let wrap_dune_expr (parameter : Ezjsonm.value) =
  let dune_expr_json_param = `O ["dune_expr",parameter] in
  Ezjsonm.value_to_string dune_expr_json_param

let wrap_string_dune_expr (parameter : string) =
  let json_param = Ezjsonm.value_from_string parameter in
  wrap_dune_expr json_param


let lovetup1 enc = enc

let lovetup2 enc1 enc2 =
  let open Json_encoding in
  conv
    (fun (e1, e2) ->
       `O ["tuple", `A [construct enc1 e1; construct enc2 e2]])
    (function
        `O ["tuple", `A [e1; e2]] ->
        (destruct enc1 e1, destruct enc2 e2)
      | _ -> raise (DecodingError "Failing when decoding a tup")
    )
    json


let lovetup3 enc1 enc2 enc3 =
  let open Json_encoding in
  conv
    (fun (e1, e2, e3) ->
       `O ["tuple", `A [construct enc1 e1; construct enc2 e2; construct enc3 e3]])
    (function
        `O ["tuple", `A [e1; e2; e3]] ->
        (destruct enc1 e1, destruct enc2 e2, destruct enc3 e3)
      | _ -> raise (DecodingError "Failing when decoding a tup")
    )
    json

let lovetup4 enc1 enc2 enc3 enc4 =
  let open Json_encoding in
  conv
    (fun (e1, e2, e3, e4) ->
      `O ["tuple", `A [construct enc1 e1; construct enc2 e2; construct enc3 e3; construct enc4 e4]])
    (function
       `O ["tuple", `A [e1; e2; e3; e4]] ->
        (destruct enc1 e1, destruct enc2 e2, destruct enc3 e3, destruct enc4 e4)
     | _ -> raise (DecodingError "Failing when decoding a tup")
    )
    json

let lovetup5 enc1 enc2 enc3 enc4 enc5 =
  let open Json_encoding in
  conv
    (fun (e1, e2, e3, e4, e5) ->
      `O ["tuple", `A [construct enc1 e1; construct enc2 e2; construct enc3 e3; construct enc4 e4; construct enc5 e5]])
    (function
       `O ["tuple", `A [e1; e2; e3; e4; e5]] ->
        (destruct enc1 e1, destruct enc2 e2, destruct enc3 e3, destruct enc4 e4, destruct enc5 e5)
     | _ -> raise (DecodingError "Failing when decoding a tup")
    )
    json

let lovetup6 enc1 enc2 enc3 enc4 enc5 enc6 =
  let open Json_encoding in
  conv
    (fun (e1, e2, e3, e4, e5,e6) ->
      `O ["tuple", `A [construct enc1 e1; construct enc2 e2; construct enc3 e3; construct enc4 e4; construct enc5 e5; construct enc6 e6]])
    (function
       `O ["tuple", `A [e1; e2; e3; e4; e5; e6]] ->
        (destruct enc1 e1, destruct enc2 e2, destruct enc3 e3, destruct enc4 e4, destruct enc5 e5, destruct enc6 e6)
     | _ -> raise (DecodingError "Failing when decoding a tup")
    )
    json

let lovetup7 enc1 enc2 enc3 enc4 enc5 enc6 enc7 =
  let open Json_encoding in
  conv
    (fun (e1, e2, e3, e4, e5, e6, e7) ->
      `O ["tuple", `A [construct enc1 e1; construct enc2 e2; construct enc3 e3; construct enc4 e4; construct enc5 e5; construct enc6 e6; construct enc7 e7]])
    (function
       `O ["tuple", `A [e1; e2; e3; e4; e5; e6; e7]] ->
        (destruct enc1 e1, destruct enc2 e2, destruct enc3 e3, destruct enc4 e4, destruct enc5 e5, destruct enc6 e6, destruct enc7 e7)
     | _ -> raise (DecodingError "Failing when decoding a tup")
    )
    json

let lovetup11 enc1 enc2 enc3 enc4 enc5 enc6 enc7 enc8 enc9 enc10 enc11 =
  let open Json_encoding in
  conv
    (fun (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11) ->
      `O ["tuple", `A [construct enc1 e1; construct enc2 e2; construct enc3 e3; construct enc4 e4; construct enc5 e5; construct enc6 e6; construct enc7 e7; construct enc8 e8; construct enc9 e9; construct enc10 e10; construct enc11 e11]])
    (function
       `O ["tuple", `A [e1; e2; e3; e4; e5; e6; e7; e8; e9; e10; e11]] ->
        (destruct enc1 e1, destruct enc2 e2, destruct enc3 e3, destruct enc4 e4, destruct enc5 e5, destruct enc6 e6, destruct enc7 e7, destruct enc8 e8, destruct enc9 e9, destruct enc10 e10, destruct enc11 e11)
     | _ -> raise (DecodingError "Failing when decoding a tup")
    )
    json

let lovetup12 enc1 enc2 enc3 enc4 enc5 enc6 enc7 enc8 enc9 enc10 enc11 enc12 =
  let open Json_encoding in
  conv
    (fun (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10,e11, e12) -> ((e1, e2, e3, e4, e5, e6, e7, e8, e9, e10),(e11, e12)))
    (fun ((e1, e2, e3, e4, e5, e6, e7, e8, e9, e10),(e11, e12)) -> (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10,e11, e12))
  @@
    merge_objs
      (tup10 enc1 enc2 enc3 enc4 enc5 enc6 enc7 enc8 enc9 enc10)
      (tup2 enc11 enc12)


let lovetup13 enc1 enc2 enc3 enc4 enc5 enc6 enc7 enc8 enc9 enc10 enc11 enc12 enc13 =
  let open Json_encoding in
  conv
    (fun (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10,e11, e12, e13) -> ((e1, e2, e3, e4, e5, e6, e7, e8, e9, e10),(e11, e12, e13)))
    (fun ((e1, e2, e3, e4, e5, e6, e7, e8, e9, e10),(e11, e12, e13)) -> (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10,e11, e12, e13))
  @@
    merge_objs
      (tup10 enc1 enc2 enc3 enc4 enc5 enc6 enc7 enc8 enc9 enc10)
      (tup3 enc11 enc12 enc13)


let lovetup14 enc1 enc2 enc3 enc4 enc5 enc6 enc7 enc8 enc9 enc10 enc11 enc12 enc13 enc14 =
  let open Json_encoding in
  conv
    (fun (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10,e11, e12, e13,e14) -> ((e1, e2, e3, e4, e5, e6, e7, e8, e9, e10),(e11, e12, e13,e14)))
    (fun ((e1, e2, e3, e4, e5, e6, e7, e8, e9, e10),(e11, e12, e13, e14)) -> (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10,e11, e12, e13, e14))
  @@
    merge_objs
      (tup10 enc1 enc2 enc3 enc4 enc5 enc6 enc7 enc8 enc9 enc10)
      (tup4 enc11 enc12 enc13 enc14)

let lovetup15 enc1 enc2 enc3 enc4 enc5 enc6 enc7 enc8 enc9 enc10 enc11 enc12 enc13 enc14 enc15 =
  let open Json_encoding in
  conv
    (fun (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10,e11, e12, e13,e14,e15) -> ((e1, e2, e3, e4, e5, e6, e7, e8, e9, e10),(e11, e12, e13,e14,e15)))
    (fun ((e1, e2, e3, e4, e5, e6, e7, e8, e9, e10),(e11, e12, e13, e14,e15)) -> (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10,e11, e12, e13, e14, e15))
  @@
    merge_objs
      (tup10 enc1 enc2 enc3 enc4 enc5 enc6 enc7 enc8 enc9 enc10)
      (tup5 enc11 enc12 enc13 enc14 enc15)


let obj11 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 =
  let open Json_encoding in
    conv
    (fun (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10,e11) -> ((e1, e2, e3, e4, e5, e6),(e7, e8, e9, e10, e11)))
    (fun ((e1, e2, e3, e4, e5, e6),(e7, e8, e9, e10, e11)) -> (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10,e11)) @@
  merge_objs (obj6 f1 f2 f3 f4 f5 f6) (obj5 f7 f8 f9 f10 f11)

let obj12 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 =
  let open Json_encoding in
    conv
    (fun (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12) -> ((e1, e2, e3, e4, e5, e6),(e7, e8, e9, e10, e11, e12)))
    (fun ((e1, e2, e3, e4, e5, e6),(e7, e8, e9, e10, e11, e12)) -> (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12)) @@
  merge_objs (obj6 f1 f2 f3 f4 f5 f6) (obj6 f7 f8 f9 f10 f11 f12)

let obj13 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 =
  let open Json_encoding in
    conv
    (fun (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13) -> ((e1, e2, e3, e4, e5, e6),(e7, e8, e9, e10, e11, e12, e13)))
    (fun ((e1, e2, e3, e4, e5, e6),(e7, e8, e9, e10, e11, e12, e13)) -> (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13)) @@
  merge_objs (obj6 f1 f2 f3 f4 f5 f6) (obj7 f7 f8 f9 f10 f11 f12 f13)

let obj14 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 =
  let open Json_encoding in
    conv
    (fun (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14) -> ((e1, e2, e3, e4, e5, e6),(e7, e8, e9, e10, e11, e12, e13, e14)))
    (fun ((e1, e2, e3, e4, e5, e6),(e7, e8, e9, e10, e11, e12, e13, e14)) -> (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14)) @@
  merge_objs (obj6 f1 f2 f3 f4 f5 f6) (obj8 f7 f8 f9 f10 f11 f12 f13 f14)

let obj15 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 =
  let open Json_encoding in
    conv
    (fun (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15) -> ((e1, e2, e3, e4, e5, e6),(e7, e8, e9, e10, e11, e12, e13, e14, e15)))
    (fun ((e1, e2, e3, e4, e5, e6),(e7, e8, e9, e10, e11, e12, e13, e14, e15)) -> (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15)) @@
  merge_objs (obj6 f1 f2 f3 f4 f5 f6) (obj9 f7 f8 f9 f10 f11 f12 f13 f14 f15)

let obj16 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16=
  let open Json_encoding in
    conv
    (fun (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16) -> ((e1, e2, e3, e4, e5, e6),(e7, e8, e9, e10, e11, e12, e13, e14, e15, e16)))
    (fun ((e1, e2, e3, e4, e5, e6),(e7, e8, e9, e10, e11, e12, e13, e14, e15, e16)) -> (e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16)) @@
  merge_objs (obj6 f1 f2 f3 f4 f5 f6) (obj10 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16)


let lovelist_encode = encode_list
let lovelist_decode = decode_list

let lovelist_encoding enc =
  let open Json_encoding in
  conv
    (fun l -> `O ["list", encode_list (construct enc) l])
    (function
        `O ["list", l] -> decode_list (destruct enc) l
      | _ -> failwith "List decoding error")
    json


(*let lovelist_encoding enc =
 Json_encoding.conv
      (lovelist_encode enc)
      (lovelist_decode enc)
*)

(*let lovelist_encode enc = (fun l -> `O ["list", encode_list (Json_encoding.construct enc) l])
let lovelist_decode enc =     (function
        `O ["list", l] -> decode_list (Json_encoding.destruct enc) l
      | _ -> failwith "List decoding error")

let lovelist_encoding enc =
  Json_encoding.conv
    (lovelist_encode enc)
    (lovelist_decode enc)
    json
*)

let loveset_encoding enc =
  Json_encoding.conv
    (fun l -> `O ["set", encode_list (Json_encoding.construct enc) l])
    (function
        `O ["set", l] -> decode_list (Json_encoding.destruct enc) l
      | _ -> failwith "List decoding error")
    json

type ('a,'b)  map = ('a * 'b) list
type ('a,'b)  bigmap = int64 option (* id of bigmap *)

let lovemap_encoding aenc benc =
    Json_encoding.conv
      (fun l ->
        `O ["map",
            encode_list
              (Json_encoding.construct (Json_encoding.tup2 aenc benc)) l])
      (function
         `O ["map", l] ->
          decode_list
            (Json_encoding.destruct (Json_encoding.tup2 aenc benc)) l
       | _ -> failwith "List decoding error")
      json

let lovebigmap_encoding _aenc _benc =
      Json_encoding.conv
        (fun i ->
          match i with
          | None -> `O ["bigmap",
                 `Null;]
          | Some i ->
             `O ["bigmap",
                 `O ["some",`String (Int64.to_string i)]]
        )
      (function
         `O l ->
          begin
            match List.assoc "bigmap" l with
            | `O l ->
               begin
                 match List.assoc "some" l with
                 | `String i -> Some (Int64.of_string i)
                 | other -> failwith (Printf.sprintf "bigmap decoding error: %s" (Ezjsonm.value_to_string other))
               end
            | other -> failwith (Printf.sprintf "Missing field bigmap in: %s" (Ezjsonm.value_to_string other))
          end
       | other -> failwith (Printf.sprintf "bigmap decoding error: %s" (Ezjsonm.value_to_string other)))
      json


let loveoption_encoding enc =
  Json_encoding.conv
    (function
        None ->   `O ["constr",`A [`String "None"; `Null]]
      | Some e ->
        `O ["constr",`A [`String "Some"; encode_list (Json_encoding.construct enc) [e]]]
    )
    (function
        `O ["constr",`A [`String "None"; `Null]] ->
        None
      | `O ["constr",`A [`String "Some"; l]] ->
        begin
          match decode_list (Json_encoding.destruct enc) l with
            [e] -> Some e
          | _ -> raise (DecodingError "Failing when decoding option: more than 1 element")
        end
      | `Null -> None
      | _ -> raise (DecodingError "Failing when decoding option")
    )
    json

let unit_encoding = Json_encoding.null

let int_encode = function i -> `O ["int", `String (string_of_int i)]
let int_decode = function | `O ["int", `String s] -> begin try int_of_string s with | _ -> raise (DecodingError "Failing when decoding int") end
                          | _ -> raise (DecodingError "Failing when decoding int")

let int_encoding =
  let open Json_encoding in
  conv int_encode int_decode json

let bool_encode = function b -> `Bool b
let bool_decode = function | `Bool b -> b
                           | _ -> raise (DecodingError "Failing when decoding bool")

let bool_encoding =
  let open Json_encoding in
  conv bool_encode bool_decode json

let timestamp_encode t = `O ["time", `String t]
let timestamp_decode = function
  | `O ["time", `String t] -> t
  | _ -> raise (DecodingError "Failing when decoding timestamp")

let timestamp_encoding =
  let open Json_encoding in
  conv timestamp_encode timestamp_decode json


let int_encode = Json_encoding.construct int_encoding
let int_decode = Json_encoding.destruct int_encoding

(* let bytes_encoding =
 *   let open Json_encoding in
 *   conv (fun b -> Hex.(show @@ of_bytes b)) (fun s -> Hex.to_bytes (`Hex s)) string *)

let bytes_encoding =
  Json_encoding.conv (fun b -> Hex.(show @@ of_bytes b)) (fun s -> Hex.to_bytes (`Hex s))
    (Json_encoding.obj1 (Json_encoding.req "bytes" Json_encoding.string))

let prenat_encode n = Z.to_string n
let prenat_decode n = Z.of_string n
let prenat_encoding = let open Json_encoding in
                      conv prenat_encode prenat_decode string

let nat_encoding =
  Json_encoding.obj1 (Json_encoding.req "nat" prenat_encoding)


(* let player_role_encoding =
 *   Json_encoding.conv
 *     (function
 *         Mastermind i ->
 *         `O ["constr",
 *             `A [
 *               `String "Mastermind";
 *               encode_list (Json_encoding.construct int_value_encoding) [string_of_int i]
 *             ]
 *            ]
 *       | Challenger i ->
 *         `O ["constr",
 *             `A [
 *               `String "Challenger";
 *               encode_list (Json_encoding.construct int_value_encoding) [string_of_int i]]
 *            ]
 *     )
 *     (function
 *         `O ["constr",
 *             `A [
 *               `String "Mastermind";
 *               l
 *             ]
 *            ] ->
 *         begin
 *           match decode_list (Json_encoding.destruct int_value_encoding) l with
 *             [elt] -> Mastermind (int_of_string elt)
 *           | _ -> raise (DecodingError "Failing when decoding Mastermind role")
 *         end
 *       | `O ["constr",
 *             `A [
 *               `String "Challenger";
 *               l
 *             ]
 *            ] ->
 *         begin
 *           match decode_list (Json_encoding.destruct int_value_encoding) l with
 *             [elt] -> Challenger (int_of_string elt)
 *           | _ -> raise (DecodingError "Failing when decoding Mastermind role")
 *         end
 *       | _ ->  raise (DecodingError "Failing when decoding role")
 *     )
 *     json *)

type address = string

let address_encode = fun s -> `O ["addr", `String s]
let address_decode = function
        `O ["addr", `String s]
      | `O ["address", `String s] -> s
      | _ -> raise (DecodingError "Failed address decoding")


let address_encoding =
  Json_encoding.conv
    address_encode
    address_decode
    json

(*let address_value_encoding = address_encoding*)

let dun_encoding =
  Json_encoding.conv
    (fun s -> `O ["dun", `String (Z.to_string s)])
    (function
      | `O ["dun", `String s] -> Z.mul (Z.of_string s) (Z.of_string "1000000")
      | `O ["mudun", `String s] -> Z.of_string s
      | _ -> raise (DecodingError "Failed dun decoding")
    )
    json

(* let dun_encoding =
 *   Json_encoding.conv
 *     (fun s -> `O ["mudun", `String s])
 *     (function
 *       | `O ["dun", `String s] -> s^"000000"
 *       | `O ["mudun", `String s] -> s
 *       | _ -> raise (DecodingError "Failed dun decoding")
 *     )
 *     json *)

let exec_parameter_encoding enc =
  Json_encoding.obj2 (Json_encoding.req "expr" enc) (Json_encoding.req "gas" Json_encoding.string)

let unit_parameter_encoding = exec_parameter_encoding unit_encoding

let id_parameter_encoding =
  exec_parameter_encoding (dune_expr int_encoding)

let address_parameter_encoding = exec_parameter_encoding address_encoding

type 'a set = 'a list

type keyhash = string
let keyhash_encoding = Json_encoding.(obj1 (req "pkh" string_encoding))
(* let keyhash_encoding = string_encoding *)

(* Examples *)

let unit_example () : unit = ()
let string_example () : string = "Dune Network"

let chooseFrom (l : (unit -> 'a) list) () : 'a =
  let n = List.length l in
  let k = Random.int n in
  List.nth l k ()

let bool_example () : bool = Random.int 2 = 0
let timestamp_example () : timestamp = "2015-12-01T10:01:00+01:00"
let int_example () : int = Random.int 10
let z_example () : Z.t = Z.of_int (int_example ())
let dun_example () : dun = z_example ()
let nat_example () : nat = z_example ()
let loveexample2 ex1 ex2 () = (ex1 (),ex2 ())
let make_list f k () =
  let rec aux res = function
    | 0 -> res
    | i -> aux ((f ())::res) (i-1)
  in aux [] k

let lovemap_example f g = make_list (loveexample2 f g) (int_example ())


let loveset_example f = make_list f (int_example ())
let lovelist_example f = make_list f (int_example ())
let bytes_example () : bytes = Bytes.make (int_example()) 'f'
let address_example () : address = "dn1dYgL65EUkPYCsqqrPCAMXHdvniERPw7CA"
let keyhash_example () : keyhash = "dn1dYgL65EUkPYCsqqrPCAMXHdvniERPw7CA"
let loveoption_example f () = chooseFrom [(fun () -> None); (fun () -> Some (f ()))] ()
let lovebigmap_example _aenc _benc = loveoption_example (fun () -> (Int64.of_int (int_example ())))
