open Lwt.Infix
open Ezjs_min

let route app path =
  log "route %s" (to_string path);
  match to_string path with
  | "/" | "" -> Core.make app
  | _ -> Lwt.return_unit

let () =
  Lwt.async (fun () ->
      Request.get_server_info () >>= fun si ->
      V.add_method1 "route" route;
      let storage = Core.init () in
      let path = List.hd Url.Current.path in
      let app =
        V.init
          ~kt1:si.info_kt1
          ~storage
          ~render:Render.home_render
          ~static_renders:Render.home_static_renders
          () in
      app##.path := string "loading";
      route app (string path))
