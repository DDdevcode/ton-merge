(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_of_ocaml_lwt.XmlHttpRequest

type 'a metal_error =
  | Xhr_err of string generic_http_frame
  | Js_err of 'a Js_of_ocaml.Js.t
  | Str_err of string
  | Gen_err of (int * string)
  | Exn_err of exn list

let (>>=) = Lwt.(>>=)
let return = Lwt.return
let return_unit = Lwt.return_unit
let async = Lwt.async

let error_content = function
  | Xhr_err f -> (f.code, f.content)
  | Js_err a -> (42, Js_of_ocaml.Js.(to_string @@ _JSON##stringify a))
  | Str_err s -> (42, s)
  | Gen_err c -> c
  | Exn_err e -> (42, String.concat "\n" @@ List.map Printexc.to_string e)

let wrap_err f = function
  | Error e -> return (Error e)
  | Ok x -> f x

let wrap_err_end f = function
  | Error e -> return (Error e)
  | Ok x -> return (Ok (f x))

let (>>?) v f =
  match v with
  | Error _ as err -> err
  | Ok v -> f v

let (>>=?) v f =
  v >>= function
  | Error _ as err -> Lwt.return err
  | Ok v -> f v

let (>>|?) v f = v >>=? fun v -> Lwt.return (Ok (f v))
let (>|=) = Lwt.(>|=)

let (>|?) v f = v >>? fun v -> Ok (f v)


let async_req ?error req update =
  async (fun () -> req >>= function
    | Error e -> (match error with
        | None -> return_unit
        | Some error -> let code, content = error_content e in
          Format.eprintf "Aync Error: %s@." content;
          return (error code content))
    | Ok res -> Lwt.return (update res))

let async_req_opt ?error ?callback req =
  async_req ?error req (match callback with None -> (fun _ -> ()) | Some callback -> callback)

let async_exn p update =
  async (fun () -> p >>= function
    | Error e -> let (code, content) = error_content e in
      Printf.eprintf "Error in async_exn %d %s\n%!" code content;
      return_unit
    | Ok x -> update x; return_unit)

let async_exn_opt ?callback p =
  async_exn p (match callback with None -> (fun _ -> ()) | Some callback -> callback)

let async0 p update =
  async (fun () -> p >>= fun x -> update x; return_unit)

let async0_opt ?callback p =
  async0 p (match callback with None -> (fun _ -> ()) | Some callback -> callback)
