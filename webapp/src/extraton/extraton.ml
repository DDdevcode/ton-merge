open Ezjs_min
open Promise

class type version = object
  method name : js_string t readonly_prop
  method version : js_string t readonly_prop
end

class type network = object
  method id : int readonly_prop
end

class type processing = object
  method wait : unit optdef promise t meth
end

class type token = object
  method wallet : wallet t readonly_prop
  method type_ : int readonly_prop
  method name : js_string t readonly_prop
  method symbol : js_string t readonly_prop
  method balance : js_string t readonly_prop
  method decimals : js_string t readonly_prop
  method rootAddress : js_string t readonly_prop
  method data : Unsafe.any readonly_prop
  method isActive : bool t readonly_prop
  method walletAddress : js_string t opt readonly_prop
  method transfer : js_string t -> js_string t -> processing t promise t meth
end

and wallet = object
  method transfer : js_string t -> js_string t -> bool t optdef -> js_string t optdef -> processing t promise t meth
  method getTokenList : token t js_array t promise t meth
end

class type signer = object
  method provider : provider t readonly_prop
  method getWallet : wallet t meth
end

and provider = object
  method getSigner : signer t promise t meth
  method getVersion : version t promise t meth
  method getNetwork : network t promise t meth
end

class type providers = object
  method _ExtensionProvider : (Unsafe.any -> provider t) constr readonly_prop
end

class type utils = object
  method stringToHex : js_string t -> js_string t meth
end

class type contract = object
  method address : js_string t readonly_prop
  method methods : 'meth t Table.t readonly_prop
end

class type extraton = object
  method providers : providers t readonly_prop
  method utils : utils t readonly_prop
  method _Contract_sign : (signer t -> Unsafe.any -> js_string t -> contract t) constr readonly_prop
  method _Contract_anon : (provider t -> Unsafe.any -> js_string t -> contract t) constr readonly_prop
end

let get_provider () =
  let extraton : extraton t = Unsafe.variable "extraton" in
  let cs = extraton##.providers##._ExtensionProvider in
  new%js cs (Unsafe.coerce Dom_html.window)##.freeton

let get_signer ?provider f =
  let p = match provider with None -> get_provider () | Some p -> p in
  jthen0 p##getSigner f

let get_version ?provider f =
  let p = match provider with None -> get_provider () | Some p -> p in
  jthen0 p##getVersion f

let network ?provider f =
  let p = match provider with None -> get_provider () | Some p -> p in
  jthen0 p##getNetwork f

let get_wallet ?signer ?provider f =
  let f s = f s##getWallet in
  match signer with
  | Some s -> f s
  | None -> get_signer ?provider f

let get_tokens ?wallet ?signer ?provider f =
  let f w = jthen0 w##getTokenList f in
  match wallet with
  | Some w -> f w
  | None -> get_wallet ?signer ?provider f

let transfer ?wallet ?signer ?provider ?payload ?bounce ~address ~amount f =
  let f w =
    let p = w##transfer (string address) (string amount)
        (optdef bool bounce) (optdef string payload) in
    jthen0 p (fun pc -> jthen0 pc##wait f) in
  match wallet with
  | Some w -> f w
  | None -> get_wallet ?signer ?provider f

let run ?provider ~abi ~address ~meth ~args f =
  let p = match provider with None -> get_provider () | Some p -> p in
  let extraton : extraton t = Unsafe.variable "extraton" in
  let cs = extraton##._Contract_anon in
  let contract = new%js cs p (Unsafe.inject abi) (string address) in
  match Table.find contract##.methods meth with
  | None -> log "method %S is not in the contract" meth
  | Some meth -> jthen0 (Unsafe.meth_call meth "run" args) f

let call ?provider ?signer ~abi ~address ~meth ~args f =
  let f s =
    let extraton : extraton t = Unsafe.variable "extraton" in
    let cs = extraton##._Contract_sign in
    let contract = new%js cs s (Unsafe.inject abi) (string address) in
    match Table.find contract##.methods meth with
    | None -> log "method %S is not in the contract" meth
    | Some meth -> jthen0 (Unsafe.meth_call meth "call" args) (fun pc -> jthen0 pc##wait f) in
  match signer with
  | Some s -> f s
  | None -> get_signer ?provider f

let catch_unit_int64 ?n symbol s =
  let n = match n with None -> String.length s | Some n -> n in
  let nsymbol = String.length symbol in
  if n > nsymbol && String.sub s (n-nsymbol) nsymbol = symbol then Some nsymbol
  else None

let units = [
  "n", 1L;
  "u", 1000L;
  "m", 1000000L;
  "", 1000000000L;
  "k", 1000000000000L;
  "M", 1000000000000000L;
  "G", 1000000000000000000L;
  ]

let ton_of_string ?(units=units) s =
  let s = String.lowercase_ascii @@ String.trim s in
  let n = String.length s in
  let tr factor s =
    let s = String.trim s in
    match Int64.of_string_opt s with
    | Some i -> Ok (Int64.mul factor i)
    | None -> match float_of_string_opt s with
      | None -> Error ("cannot extract ton from " ^ s)
      | Some f -> Ok (Int64.of_float (f *. (Int64.to_float factor))) in
  let tr_sub factor s n i = tr factor (String.sub s 0 (n-i)) in
  let l =
    List.map (fun (c, factor) ->
        catch_unit_int64 ~n (c ^ "ton"), factor) units in
  let rec iter = function
    | [] -> tr_sub 1L s n 0
    | (h, f) :: t -> match h s with
      | None -> iter t
      | Some i ->
        match tr_sub f s n i with
        | Error _ -> iter t
        | Ok i -> Ok i in
  iter l

let sep =
  try match to_string (number_of_float 0.1)##toLocaleString with
    | "0,1" -> ','
    | _ -> '.'
  with _ -> '.'

let non_zeroes s =
  let rec iter s n = match s.[n-1] with
    | '0' -> iter s (n-1)
    | '.' | ',' -> n-1
    | _ -> n in
  iter s (String.length s)

let pp_amount ?(precision=6) ?(width=15) ?order amount =
  if amount = 0L then "0", 3, None
  else
    let sign = if amount < 0L then "-" else "" in
    let volumeL = Int64.abs amount in
    let ndecimal = String.length (Int64.to_string volumeL) in
    let diff_length = ndecimal - width in
    let order = match order with
      | None ->
        if ndecimal > 9 then
          max 3 ((ndecimal - width + 2) / 3)
        else if ndecimal > 6 then
          max 2 ((ndecimal - width + 2) / 3)
        else if ndecimal > 3 then
          max 1 ((ndecimal - width + 2) / 3)
        else
          max 0 ((ndecimal - width + 2) / 3)
      | Some order -> order in
    let unit_float = if diff_length < 0 then 1L else
        Int64.(of_float (10. ** (float_of_int diff_length))) in
    let unit_int = if order < 0 then 1L else
        Int64.(of_float (10. ** (float_of_int (order * 3 )))) in
    let volume_float = Int64.div volumeL unit_float in
    let volume_int = Int64.div volumeL unit_int in
    let decimal = Int64.sub (Int64.mul volume_float unit_float)
        (Int64.mul volume_int unit_int) in
    let volume_int, decimal, precision =
      if order * 3 < precision then volume_int, decimal, order * 3 else
        let unit_precision = Int64.of_float (10. ** float_of_int (order * 3 - precision)) in
        let tmp = Int64.div decimal unit_precision in
        let unit_precision2 = Int64.div unit_precision 10L in
        if unit_precision2 = 0L || tmp = 0L ||
           Int64.div (Int64.sub decimal (Int64.mul tmp unit_precision))
             (Int64.div unit_precision 10L) < 5L then
          volume_int, tmp, precision
        else if Int64.succ tmp = Int64.of_float (10. ** float_of_int precision) then
          Int64.succ volume_int, Int64.zero, precision
        else
          volume_int, Int64.succ tmp, precision
    in
    let num = to_string
        ((number_of_float
            (Int64.to_float volume_int))##toLocaleString ) in
    let decimal_str = Printf.sprintf "%c%0*Ld" sep precision decimal in
    let n = non_zeroes decimal_str in
    let decimal_str = String.sub decimal_str 0 n in
    sign ^ num, max order 0,
    if decimal <> 0L then Some decimal_str else None
