
type provider
type signer

val units : (string * int64) list

val pp_amount : ?precision:int ->
  ?width:int -> ?order:int -> int64 -> string * int * string option

val call :
  ?provider:provider Js_of_ocaml.Js.t ->
  ?signer:signer Ezjs_min.t ->
  abi:'a ->
  address:string ->
  meth:string ->
  args:Ezjs_min.Unsafe.any array -> ('b -> unit) -> unit

val get_provider : unit -> provider Js_of_ocaml.Js.t
val get_signer :
  ?provider:provider Js_of_ocaml.Js.t ->
  (signer Ezjs_min.t -> unit) -> unit
