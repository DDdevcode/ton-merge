# Documentation

This directory contains two PDF files:
* Our submission to the architecture contest
* Our submission to the implementation contest

Note that the implementation differs slightly from the architecture,
by mainly two points:

* The DuneGiver, DuneRootSwap and DuneEvents contracts have been merged into
only one contract, the DuneRootSwap contract;

* The DuneUserSwap contract only manages one swap order, instead of all
the swap orders of a given user; so a user creating multiple swap orders will
trigger the creation of multiple DuneUserSwap contracts for each of them;

