open Encoding_common
open Encoding
open EzAPI
open Data_types

module Encoding = struct
  include Encoding_common
  include Encoding
end

module Params = struct
  let origin = Param.string ~required:true ~descr:"Origin of swap" "origin"
  let confirmed =
    Param.bool ~required:false ~descr:"filter on confirmation status" "confirmed"
  let status =
    Param.string ~required:false ~descr:"filter on operation status" "status"
      ~examples:["queued"; "pending"; "unconfirmed"; "confirmed"; "error"]
  let kind =
    Param.string ~required:false ~descr:"filter on operation kind" "kind"
      ~examples:operation_kinds
  let swap_id =
    Param.string ~required:false ~descr:"swap number" "swap_id"
end

module Args = struct
  let swap_id = Arg.int ~example:1 ~descr:"ID of swap" "swap_id"
end


let misc_section = Doc.section "Misc Requests"
let swaps_section = Doc.section "Swaps Requests"
let sections = [
  misc_section;
  swaps_section;
]

let errors = [
  Err.make ~code:500 ~name:"error"
    ~encoding:Json_encoding.any_ezjson_value
    ~select:(fun x -> x) ~deselect:(fun x -> Some x)
]

let server_info : (_, _, no_security) service0 =
  service
    ~section:misc_section
    ~name:"Server Info"
    ~descr:"Show various information about server, like the version \
            of the database, the software revision and date, etc."
    ~output:server_info
    ~errors
    Path.(root // "server_info")

let list_swaps : (_, _, no_security) service0 =
  service
    ~section:swaps_section
    ~name:"List swaps by origin"
    ~descr:"List all swaps by origin"
    ~params:[Params.origin]
    ~output:(Json_encoding.list swap)
    ~errors
    Path.(root // "swaps")

let list_events : (_, _, no_security) service0 =
  service
    ~section:swaps_section
    ~name:"List events by origin"
    ~descr:"List all swaps by origin"
    ~params:[Params.swap_id]
    ~output:Json_encoding.(list (tup2 string string))
    ~errors
    Path.(root // "list_events")

let list_operations : (_, _, no_security) service0 =
  service
    ~section:swaps_section
    ~name:"List operations by origin"
    ~descr:"List all operations by origin"
    ~params:[Params.origin]
    ~output:Json_encoding.(list (operation (option block)))
    ~errors
    Path.(root // "list_operations")

let swapper_info : (_, _, no_security) service0 =
  service
    ~section:swaps_section
    ~name:"Get swapper info"
    ~descr:"Return swapper info and current state"
    ~output:swapper_info
    ~errors
    Path.(root // "swapper_info")

let get_swap : (int, swap, _, no_security) service1 =
  service
    ~section:swaps_section
    ~name:"Get a swap"
    ~descr:"Retrieve information for a registered swap"
    ~output:swap
    ~errors
    Path.(root // "swaps" /: Args.swap_id )

let reveal_secret : (int, bytes, unit, _, no_security) post_service1 =
  post_service
    ~section:swaps_section
    ~name:"Reveal a secret"
    ~descr:"Reveal secret for a given swap"
    ~input:hex_bytes
    ~output:Json_encoding.unit
    ~errors
    ~input_example:(Bytes.of_string "Bonjour")
    ~output_example:()
    Path.(root // "swaps" /: Args.swap_id // "reveal_secret")

let get_swap_contract : (int, string, _, no_security) service1 =
  service
    ~section:swaps_section
    ~name:"Get freeton swap contract address"
    ~descr:"Retrieve the address of the swap contract on freeton chain for a given swap"
    ~output:Json_encoding.string
    ~errors
    Path.(root // "contract" /: Args.swap_id )

(* modify api.ml *)
