open Lwt_utils
open Data_types
open Blockchain_common

module MetalForge = Metal.Forge
(* open Metal.Dune.Types *)
open Spice_swapper

let op_max_length = 16384

let debug_flag = ref false

let debug fmt =
  if !debug_flag then Format.eprintf fmt
  else Format.ifprintf Format.err_formatter fmt

let node_url () =
  Config.get_nodes () >|= List.hd


let get_source_keys ~source =
  match Sys.getenv_opt "DUNE_INJECTOR_SK" with
  | None | Some "" -> failwith "Set DUNE_INJECTOR_SK environment variable"
  | Some sk ->
    let sk = Crypto.Secret_key.of_b58 sk in
    let pk = Crypto.Secret_key.to_public_key sk in
    let pkh = Crypto.Public_key.hash pk in
    let pkh_b58 = Crypto.Public_key_hash.to_b58 pkh in
    (match source with
     | Some s when s <> pkh_b58 ->
       Format.ksprintf failwith "Unexpected key %s but want source %s" pkh_b58 s
     | _ -> ()
    );
    (sk, pk, pkh)

let error s = return_error (Str_err s)

let code_of_metal_error = function
  | Gen_err (c, _) -> Some c
  | Str_err _
  | Exn_err _ -> None

let msg_of_metal_error = function
  | Str_err s -> s
  | Gen_err (c, (None | Some "")) -> "HTTP error: code " ^ string_of_int c
  | Gen_err (_, Some s) -> s
  | Exn_err l -> String.concat "\n" @@ List.map Printexc.to_string l

let lift_metal_error x = x >>= function
  | Error e -> Lwt.return_error (Format.sprintf "Metal error: %s" (msg_of_metal_error e))
  | Ok r -> Lwt.return_ok r

let print_err fmt e =
  Format.fprintf fmt "Injector error: %s" e

let rec json_find path json = match path with
  | [] -> json
  | `F field :: path -> Ezjsonm.find json [field] |> json_find path
  | `I index :: path ->
    List.nth
      (Ezjsonm.get_list (fun x -> x) json)
      index
    |> json_find path


module BuildOp = struct

  let get_src source =
    let (_, _, src) = get_source_keys ~source in
    Crypto.Public_key_hash.to_b58 src

  let get_swap_kt1 () =
    Db.get_swap_contract_address () >|= function
    | None -> failwith "Swap contract not deployed"
    | Some kt1 -> kt1

  let reveal_secret ~source ~swap_id ~secret =
    let src = get_src source in
    let> kt1 = get_swap_kt1 () in
    Swap_contract.swap_entry ~kt1 ~src (Z.of_int swap_id) secret
    |> Option.some
    |> Lwt.return_ok

  let refund ~source ~swap_id =
    let src = get_src source in
    let> kt1 = get_swap_kt1 () in
    Swap_contract.refund_entry ~kt1 ~src (Z.of_int swap_id)
    |> Option.some
    |> Lwt.return_ok

  let make op =
    let source = op.op_source in
    match op.op_content with
    | Swap { swap_id; secret } ->
      reveal_secret ~source ~swap_id ~secret
    | Refund { swap_id } ->
      refund ~source ~swap_id
    | _ ->
      (* Ignoring other operations, not injected for now *)
      Lwt.return_ok None


  let prepare_batches () =
    debug "Preparing batches@.";
    let> ops_batches = Db.get_queued_operations () in
    debug "Trying %d batches: [" (List.length ops_batches) ;
    List.iter (fun b -> debug "%d, " (List.length b)) ops_batches;
    debug "]@.";
    Lwt_list.filter_map_s (function batch ->
        Lwt_list.filter_map_s (fun op ->
            Lwt.catch
              (fun () ->
                 make op >|= function
                 | Ok Some mop -> Some (op, mop)
                 | Ok None -> None
                 | Error e ->
                   debug "Postponing operation %ld:\n%a\n@." op.op_id
                     print_err e;
                   None
              ) (fun exn ->
                  (* postpone operations which we cannot build right now *)
                  debug "Postponing operation %ld (exn):\n%s\n@." op.op_id
                    (Printexc.to_string exn);
                  Lwt.return_none)
          ) batch >|= function
        | [] -> None
        | ({ op_source; _ }, _) :: _ as batch -> Some (op_source, batch)
      ) ops_batches
    >|= fun batches ->
    (* let batches = isolate_deploys batches in *)
    (* let batches = separate_create_edition_retrieve_royalties batches in *)
    debug "Produced %d batches: [" (List.length batches) ;
    List.iter (fun (_, b) ->
        debug "\n%d: \n" (List.length b);
        List.iter (fun (o, _) ->
            debug "    - [%ld] %s\n" o.op_id @@
            Ezjsonm.value_to_string
              (Json_encoding.construct
                 Encoding_common.operation_content o.op_content);
          ) b) batches;
    debug "]@.";
    batches

end

module Forge = struct

  let too_many_operations_regex =
    Str.regexp {|node\.prevalidation\.too_many_operations|}
  let oversized_operation_regex =
    Str.regexp {|node\.prevalidation\.oversized_operation|}
  let batch_too_large_regex =
    Str.regexp @@ String.concat "\\|" [
      {|node\.prevalidation\.too_many_operations|};
      {|node\.prevalidation\.oversized_operation|};
    ]
  let proto_regex = Str.regexp {|proto\.|}
  let permanent_regex = Str.regexp {|"permanent"|}
  let temporary_regex = Str.regexp {|"temporary"|}
  let kind_regex = Str.regexp {|"kind"|}

  let match_err msg r =
    try ignore(Str.search_forward r msg 0); true
    with _ -> false

  let filter_out_failing_ops ~get_pk batch =
    let> node_url = node_url () in
    Lwt_list.filter_map_p (fun ((ton_merge_op, metal_op) as op) ->
        Metal.forge_manager_operations  (* ~remove_failed:true *)
          ~base:node_url ~get_pk ~src:metal_op.src [metal_op.op] >>= function
        | Ok _ -> Lwt.return_some op
        | Error err ->
          let err_msg = msg_of_metal_error err in
          debug "@[<v 8>Filtering out operation %ld:@,%s]@." ton_merge_op.op_id err_msg;
          if match_err err_msg permanent_regex then
            let> _ignore_err =
              Db.mark_operation_error ton_merge_op.op_id
                ("Blockchain error:\n" ^ err_msg)
                `Permanent ~retry_nb:20 in
            Lwt.return_none
          else
            let> _ignore_err =
              Db.mark_operation_error ton_merge_op.op_id
                ("Blockchain error:\n" ^ err_msg)
                `Temporary ~retry_nb:20 in
            Lwt.return_none
      ) batch

  let maybe_forge_batch ~get_pk
      (batch : (unit operation * Spice_swapper.spice_operation) list) =
    let> node_url = node_url () in
    let do_trim = ref None in
    let rec try_forge_batch batch =
      debug "Try forging batch with %d operations@." (List.length batch);
      (* FIXME: Filter out failing operations (may be costly) *)
      let> batch = match !do_trim with
        | None | Some false -> Lwt.return batch
        | Some true ->
          debug "Triming failing operations from batch@.";
          do_trim := Some false; (* Prevent second trimming *)
          filter_out_failing_ops ~get_pk batch
      in
      match batch with
      | [] -> Lwt.return None
      | first_op :: _ ->
        let src = (snd first_op).src in
        let r_ton_merge_batch, r_metal_batch = List.fold_left (fun (acc_d, acc_m) (d, m) ->
            d :: acc_d, m.op :: acc_m
          ) ([], []) batch in
        let ton_merge_batch = List.rev r_ton_merge_batch in
        let metal_batch = List.rev r_metal_batch in
        Metal.forge_manager_operations (* ~remove_failed:true *)
          ~base:node_url ~get_pk ~src metal_batch >>= function
        | Ok fg
          when Bigstring.length fg.Metal_types.fg_bytes <= op_max_length ->
          debug "Batch for %s forged successfully@." src;
          Lwt.return (Some (fg, (src, ton_merge_batch)))
        | Ok fg ->
          debug "Batch for %s is too large (%d)@." src
            (Bigstring.length fg.Metal_types.fg_bytes);
          (match List.rev batch with
           | [] -> assert false
           | [op, _] ->
             (* TODO: mark operation as too large *)
             let err =
               Printf.sprintf "Operation is too large (%d)"
                 (Bigstring.length fg.Metal_types.fg_bytes)
             in
             debug "Marking operation %ld as failed@." op.op_id;
             let> _ignore_err =
               Db.mark_operation_error op.op_id err
                 `Temporary ~retry_nb:20 in
             Lwt.return None
           | _ :: rbatch ->
             (* Drop last operation of batch *)
             try_forge_batch (List.rev rbatch)
          )
        | Error err ->
          let err_msg = msg_of_metal_error err in
          debug "Forge of batch for %s failed:\n%s@."
            src err_msg;
          if match_err err_msg too_many_operations_regex then
            let () = debug "Too many operations in mempool@." in
            (* Too many operations in mempool *)
            match List.rev batch with
            | [] | [_] ->
              (* No operations to remove, skip batch *)
              Lwt.return None
            | _ :: rbatch ->
              (* Drop last operation of batch *)
              try_forge_batch (List.rev rbatch)
          else
          if match_err err_msg oversized_operation_regex ||
             code_of_metal_error err = Some 413 (* Payload too large *)
          then
            let () = debug "Batch is too large@." in
            match List.rev batch with
            | [] -> assert false
            | [op, _] ->
              (* TODO: mark operation as too large *)
              let err = Printf.sprintf "Operation is too large\n%s" err_msg in
              debug "Marking operation %ld as failed@." op.op_id;
              let> _ignore_err =
                Db.mark_operation_error op.op_id err
                  `Temporary ~retry_nb:20 in
              Lwt.return None
            | _ :: rbatch ->
              (* Drop last operation of batch *)
              try_forge_batch (List.rev rbatch)
          else
            let () = debug "Likely protocol error@." in
            match List.rev batch with
            | [] -> assert false
            | [op, _] ->
              (* Unknown error, TODO: maybe parse *)
              debug "Marking operation %ld as failed@." op.op_id;
              let> _ignore_err =
                Db.mark_operation_error op.op_id err_msg
                  `Temporary ~retry_nb:20 in
              Lwt.return None
            | _ :: rbatch ->
              (* Drop last operation of batch *)
              if !do_trim = None then do_trim := Some true;
              (* Trim operations if never done *)
              try_forge_batch (List.rev rbatch)
    in
    try_forge_batch batch


  let unsigned_batches () =
    let> batches = BuildOp.prepare_batches () in
    debug "Forging batches@.";
    Lwt_list.filter_map_p (fun (src, batch) ->
        let (_, pk, _) = get_source_keys ~source:src in
        let get_pk () = Lwt.return_ok (Crypto.Public_key.to_b58 pk) in
        maybe_forge_batch ~get_pk batch
      ) batches

end

module Injection = struct

  let sign ~src bytes =
    let (sk, _, _) = get_source_keys ~source:src in
    match Crypto.Secret_key.sign ~sk bytes with
    | None -> error "Error signing"
    | Some signature -> return_ok signature

  let add_indexes batch ton_merge_operations =
    let rec aux acc index ops d_ops =
      match ops, d_ops with
      | Dune.Types.NReveal _ :: ops, _ -> aux acc (index + 1) ops d_ops
      | [], [] -> List.rev acc
      | _ :: ops, d :: d_ops -> aux ((index, d) :: acc) (index + 1) ops d_ops
      | _ :: _, [] | [], _ :: _ ->
        failwith "Could not match forged operations with Ton_Merge operations"
    in
    aux [] 0 batch.Metal_types.fg_ops ton_merge_operations

  let inject_queued_operations () =
    let> batches = Forge.unsigned_batches () in
    let> node_url = node_url () in
    Res.iter_p (fun (batch, (src, ton_merge_operations)) ->
        debug "Starting injection of batch for %s (%d ops)@."
          src (List.length ton_merge_operations);
        debug "Injecting signed operation@.";
        let@ op_bytes =
          lift_metal_error @@
          Metal.inject ~base:node_url ~sign:(sign ~src:(Some src)) batch in
        let op_hash = MCrypto.Operation_hash.b58enc op_bytes in
        debug "Injected operation with hash %s@." op_hash;
        debug "Updating DB for operation hash %s@." op_hash;
        let ton_merge_operations = add_indexes batch ton_merge_operations in
        let>+ () = Db.update_pending_batch ~op_hash ton_merge_operations in
        debug "Update for operation hash %s successful@." op_hash;
        ()
      ) batches
    >>|? fun () ->
    debug "Injections done@.";
    List.length batches

end
