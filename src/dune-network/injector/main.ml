open Lwt_utils
open CalendarLib

let wait_after_error = 10. (* seconds *)

(* To do at each injector loop *)
let do_each_loop () =
  (* Queue refund operations for expired swaps *)
  Db.SWAPS.refund_expired_swaps ()

let injector () =
  let name = "dune_injector" in
  let> not_running = Db.PID.check name in
  if not not_running then begin
    Printf.eprintf "Exiting.\n%!";
    Lwt.return_unit
  end else
    let> () = Db.PID.register name in
    Format.eprintf "Starting injector bot@.";
    let> block_time = Config.get_block_time () in
    let rec injector_loop () =
      Printf.eprintf ".%!";
      let> () = do_each_loop () in
      let> nb_queued_ops = Db.count_queued_operations () in
      if nb_queued_ops = 0 then begin
        Lwt_unix.sleep 5.>>= fun () ->
        injector_loop ()
      end else
        let time_start_loop = Calendar.now () in
        time_start_loop
        |> Printer.Calendar.to_string
        |> Format.eprintf "\n%s: New injection loop@.";
        Injector.Injection.inject_queued_operations () >>= function
        | Error e ->
          let err_msg =
            Printf.sprintf "INJECTOR BOT ERROR: %s" e
          in
          handler err_msg
        | Ok 0 ->
          Lwt_unix.sleep 5.>>= fun () ->
          injector_loop ()
        | Ok _ ->
          Format.eprintf
            "Waiting %d seconds for next injection...@." block_time;
          Lwt_unix.sleep @@ float_of_int block_time >>= fun () ->
          injector_loop ()
    and handler msg =
      Format.eprintf "%s@." msg;
      Format.eprintf "RESTARTING BOT IN %f seconds...@." wait_after_error;
      Lwt_unix.sleep wait_after_error >>= fun () ->
      run ()
    and run () =
      Lwt.catch
        injector_loop
        (fun exn ->
           Printexc.to_string exn
           |> Printf.sprintf "UNHANDLED BOT ERROR: %s"
           |> handler)
    in
    run () >>= fun () ->
    Format.eprintf "Unexpected temination, exiting...@.";
    exit 1

let check_env () =
  let (_, _, pkh) = Injector.get_source_keys ~source:None in
  Format.eprintf "Injectori signer account: %s@."
    (Crypto.Public_key_hash.to_b58 pkh)

let main () =
  check_env ();
  Lwt_main.run (injector ())

let () = main ()
