(**************************************************************************)
(*                                                                        *)
(*    Copyright 2021 OCamlPro                                             *)
(*                                                                        *)
(*  All rights reserved. This file is distributed under the terms of the  *)
(*  GNU Lesser General Public License version 2.1, with the special       *)
(*  exception on linking described in the file LICENSE.                   *)
(*                                                                        *)
(**************************************************************************)

open Lwt_utils
open Data_types

type action =
  | No_action
  | Daemon
  | Close_swap of string
  | Monitor_database of string
  | Monitor_freeton
  | Set_secret of int * string
  | Get_address of int * string
  | Set_config of string
  | Kill
  | Swap_info of int
  | Merge_info
  | Watch_db
  | Checks
  | Clean_freeton_db

(* These are tests using dune-ix and ft configs for sandboxing.
   swap_100 is a swap of dune-ix user0 to fr user0 of 10 DUN *)
let test_swaps =
  let secret = "Bonjour" in
  let hashed_secret =
    Hex.to_bytes
      (`Hex
         "9172e8eec99f144f72eca9a568759580edadb2cfd154857f07e657569493bc44")
  in
  let swap =     {
      swap_id = 0;
      dune_origin = "";
      dun_amount = Z.of_string "90_000_000";
      ton_amount = Z.zero; (* not used *)
      dune_status = SwapSubmitted ;
      freeton_status = SwapWaitingForConfirmation;
      hashed_secret ;
      time = Encoding_common.subst_date_time ; (* not used *)
      refund_address =  "";
      freeton_pubkey = "";
      freeton_address = "";
      freeton_depool = None;
      secret = Some (Bytes.of_string secret) ;
      swap_hash = None ;
      confirmations = 0 ;
      logical_time = 0L ;
    }
  in
  List.mapi (fun i dune_origin ->
      { swap with
        swap_id = 100 + i;
        dune_origin ;
        dun_amount = Z.of_string (Printf.sprintf "%d_000_000_000" ((i+1)*10));
        freeton_pubkey = Printf.sprintf "%%{account:pubkey:user%d}" i;
        freeton_address = Printf.sprintf "%%{account:address:user%d}" i;
      })
    [
      "dn1QudFDa1k15xwnXWYxgMRJf4RGJV1yzDym" ;
      "dn1YJFYFw6ER8GFct3JA6ktRT1pjynV1juCy" ;
      "dn1KhdkBRSRdPUVtXZaz9kNMX3nxd92LVBwH" ;
      "dn1HEggbbpWYovvKzpnBfpSN82fip56fk5K2" ;
      "dn1ZFsha5jeedydCiUxj3rLNKoKN7dmymdNg" ;
      "dn1NL8VZb7GeWCV8RYA6b3JnEDnh9B7Z2XjT" ;
      "dn1Gc5pFRGzBsnTifsNogmhiQcPHvcehbX4s" ;
      "dn1SVEoV68ofifURFHeXHVRuDHETmv3HedRa" ;
      "dn1d8CXw3GEXKcZvBYzNFWgv4jDEojRXArZo" ;
      "dn1aMH2u9D53RM74UeosvYh2c3L2k49H7iQa"
    ]

let fake config swaps =
  match swaps with
  | [] -> Lwt.return_unit
  | _ ->
    let client = Ton_sdk.CLIENT.create config.network_url in
    Lwt_list.iter_s (fun file ->
        let s = EzFile.read_file file in
        Printf.eprintf "s = %s\n%!" s;
        let s = EzEncoding.destruct Encoding_common.swap s in
        let ton_amount = Data_types.ton_of_dun s.dun_amount in
        let refund_address = if s.refund_address = "" then
            s.dune_origin else s.refund_address in
        Printf.eprintf "Add swap in DB\n%!";

        begin match s.secret with
          | None -> ()
          | Some secret ->
            assert ( Db.check_secret s secret )
        end;

        let> res = Db.SWAPS.get ~swap_id: s.swap_id in
        match res with
        | None ->
          let swap =  { s with dune_status = SwapConfirmed ;
                               ton_amount ; refund_address } in
          let> res = Freeton.get_swap_hash ~client ~config swap in
          begin
            match res with
            | None ->
              Printf.eprintf "Could not retrieve swap hash\n%!";
              exit 2
            | Some swap_hash ->
              let> () = Db.SWAPS.add swap in
              let oc = open_out (file ^ ".hash") in
              Printf.fprintf oc "%s\n%!" swap_hash;
              close_out oc;
              Lwt.return_unit
          end
        | Some _ ->
          Printf.eprintf "Swap %d is already in database\n%!" s.swap_id;
          Lwt.return_unit
      ) swaps


let () =
  let action = ref No_action in
  let dune_swaps = ref [] in
  let only_freeton = ref false in

  Arg.parse [

    "--init-db", Arg.Unit (fun () ->
        let dbh = PGOCaml.connect ~database:(Project_config.database()) () in
        EzPG.upgrade_database ~upgrades:Db_common.VERSIONS.(!upgrades) dbh;
        Printf.eprintf "Upgrade done\n%!";
        exit 0
      ),
    " Init database";

    "--save-test-swaps", Arg.Unit (fun () ->
        List.iteri (fun i s ->
            let s = EzEncoding.construct Encoding_common.swap s in
            EzFile.write_file
              (Printf.sprintf "test-swap-%d.json.in" (100 + i)) s
          ) test_swaps ;
        Printf.eprintf "Test swaps saved to files.\n%!";
        exit 0
      ),
    " Save test swaps to files";

    "--test-swap", Arg.String (fun s ->
        dune_swaps := s :: !dune_swaps
      ),
    "SWAP.json Load a swap from a file (testing)";

    "--set-secret", Arg.String (fun s ->
        let swap_id, secret = EzString.cut_at s ':' in
        let swap_id = int_of_string swap_id in
        action := Set_secret ( swap_id, secret );
      ), "SWAP_ID:SECRET Set secret for swap id";

    "--get-address", Arg.String (fun s ->
        let swap_id, file = EzString.cut_at s ':' in
        let swap_id = int_of_string swap_id in
        action := Get_address ( swap_id, file );
      ), "SWAP_ID:FILE Lookup SWAP_ID in DB and store contract's address in FILE";

    "--daemon", Arg.Unit (fun () -> action := Daemon ),
    "Start the relay (two processes in the background)";

    "--monitor-database", Arg.String (fun s -> action := Monitor_database s ),
    "ROLE Start the relay to monitor the database";

    "--monitor-freeton", Arg.Unit (fun () -> action := Monitor_freeton ),
    "Start the relay to monitor freeton";

    "--set-config", Arg.String (fun s -> action := Set_config s ),
    "CONFIG_FILE Set the initial configuration";

    "--close-swap", Arg.String (fun s -> action := Close_swap s ),
    "RELAY_PASSPHRASE Close swap" ;

    "--only-freeton", Arg.Set only_freeton,
    " Only start FreeTON daemons";

    "--kill", Arg.Unit ( fun () -> action := Kill ),
    " Kill all managed processes and monitor" ;

    "--swap-info", Arg.Int (fun s -> action := Swap_info s ),
    "SWAP_ID Show information on swap" ;

    "--merge-info", Arg.Unit (fun () -> action := Merge_info ),
    " Show information on merge" ;

    "--watch-db", Arg.Unit (fun () -> action := Watch_db ),
    " Watch DB" ;

    "--checks", Arg.Unit (fun () -> action := Checks ),
    " Perform various checks" ;

    "--clean-freeton-db", Arg.Unit ( fun () -> action := Clean_freeton_db ),
    " Clean Freeton DB before new --set-config";

  ] (fun s ->
      Printf.eprintf "Error: unexpected argument %S\n%!" s;
      exit 2)
    "FreeTON part of the bridge\nUsage:\n\tton-merge-freeton --help";
  try
    match !action with

    | Daemon -> Daemon.start_daemon ~only_freeton:!only_freeton ()

    | Monitor_database role_name ->
      let role = Monitor_database.ROLE.of_string role_name in
      let name = "monitor_database" ^
                 Monitor_database.ROLE.to_suffix role in
      Lwt_main.run (
        let> config = Config.get_config () in
        let> not_running = Db.PID.check name in
        if not_running then
          let> () = Db.PID.register name in
          Monitor_database.monitor config role
        else
          Lwt.return_unit
      )

    | Monitor_freeton ->
      let name = "monitor_freeton" in
      Lwt_main.run (
        let> config = Config.get_config () in
        let> not_running = Db.PID.check name in
        if not_running then
          let> () = Db.PID.register name in
          Monitor_freeton.monitor config
        else
          Lwt.return_unit
      )

    | Set_secret (swap_id, secret) ->
      Lwt_main.run (
        let> ok = Db.SWAPS.set_secret ~swap_id (Bytes.of_string secret) in
        if ok then
          Printf.eprintf "Secret entered in DB\n%!";
        Lwt.return_unit
      )

    | Get_address (swap_id, file) ->
      Lwt_main.run (
        let rec iter () =
          let> swap = Db.SWAPS.get ~swap_id in
          match swap with
          | None ->
            Printf.eprintf "No swap %d\n%!" swap_id;
            exit 2
          | Some swap ->
            match swap.swap_hash with
            | None ->
              let> () = Lwt_unix.sleep 1.0 in
              iter ()
            | Some swap_hash ->
              let> res = Db.CONTRACTS.get ~swap_hash in
              match res with
              | None ->
                let> () = Lwt_unix.sleep 1.0 in
                iter ()
              | Some address ->
                EzFile.write_file file address;
                Lwt.return_unit
        in
        iter ()
      )

    | Close_swap passphrase ->
      Lwt_main.run (
        let> config = Config.get_config () in
        Close_swap.close_swap config passphrase )

    | Set_config filename ->
      Lwt_main.run (
        let> config = Config.set_config ~filename in
        if config.reveal_secrets then begin
          let setup_json =  "freeton-setup.json" in
          EzFile.write_file setup_json
            ( Printf.sprintf
                {|{
              "root_address" : "%s",
              "network_url" : "%s",
              "reveal_secrets" : false
            }|}
                config.root_address
                config.network_url )
          ;
          Printf.eprintf "File %S generated, distribute it to other relays\n%!"
            setup_json ;
        end;

        Printf.eprintf "Config set. Exiting.\n%!";
        Lwt.return_unit
      )

    | No_action ->
      Lwt_main.run (
        let> config = Config.get_config () in
        let> () = fake config !dune_swaps in
        Printf.eprintf "Nothing to do. Exiting.\n%!";
        Lwt.return_unit
      )

    | Kill ->
      Lwt_main.run (
        Lwt_list.iter_s (fun name ->

            Lwt.catch (fun () ->
                let> pid = Db.PID.get name in
                begin
                  match pid with
                  | None -> ()
                  | Some pid ->
                    Printf.eprintf "Killing %s at %d\n%!" name pid;
                    Unix.kill pid Sys.sigint ;
                end;
                Lwt.return_unit
              )
              (fun _exn -> Lwt.return_unit )
          ) Admin.freeton_names
      )

    | Swap_info swap_id ->
      Lwt_main.run ( Info.swap_info ~swap_id )

    | Merge_info ->
      Lwt_main.run ( Info.merge_info () )

    | Watch_db ->
      Lwt_main.run ( Watch.watch_db () )

    | Checks ->
      Lwt_main.run ( Admin.checks () )

    | Clean_freeton_db ->
      Lwt_main.run
        (
          let> () = Db.CONFIRMATIONS.reset () in
          let> () = Db.CONTRACTS.reset () in
          let> () = Db.CONFIG.reset () in
          let> () = Db.EVENTS.reset () in
          let> () = Db.PINGS.reset () in
          let> () = Db.TRANSACTIONS.reset () in
          Printf.eprintf "FreeTON DB cleaned\n%!";
          Lwt.return_unit
        )


  with
    Failure s ->
    Format.eprintf "Error: %s@." s;
    exit 2
