
pragma ton-solidity >= 0.37.0;

pragma AbiHeader expire;
pragma AbiHeader time;
pragma AbiHeader pubkey;

import "../interfaces/IDuneVesting.sol";
import "../interfaces/IDePool.sol";
import "../interfaces/IParticipant.sol";

import "../libraries/DuneRelayBase.sol";
import "../libraries/DuneConstBase.sol";


contract DuneVesting is IDuneVesting, DuneConstBase, IParticipant
{

  /*********************************************************************/

  //                          CONSTANTS

  /*********************************************************************/

  uint64 constant MERGE_EXPIRATION_DATE = %{time:2022-07-01};
  uint64 constant AUTO_UNLOCK_DATE = %{time:2022-01-01};
  uint64 constant AUTO_MAXBONUS_DATE = %{time:2022-04-01};
  uint32 constant MIN_VESTING_PERIOD = 3 * MONTH_SECONDS ;
  uint32 constant MAX_VESTING_PERIOD = 6 * MONTH_SECONDS ; 

  uint128 constant MIN_STAKE = 100 ton ;
  
  uint256 constant GIVER_MSIG_ADDR =
    0x1e47ab9136f03d6dd2e66e5e8f901e529e51e6be9cd0b5720cc72b717e167a0b ;
  
  /*********************************************************************/

  //                          STATIC VARIABLES

  /*********************************************************************/

  // Use the pubkey of the user

  /*********************************************************************/

  //                          VARIABLES

  /*********************************************************************/

  uint8 g_version = VERSION;
  uint128 g_bonus ;
  uint128 g_amount ;
  uint64 g_deploy_date ;
  mapping( address => bool ) is_for6months ; // depool -> for6months
  
  /*********************************************************************/

  //                          CONSTRUCTOR

  /*********************************************************************/

  constructor()
  {
    tvm.accept();
    g_deploy_date = now ;
    _refill( address(this).balance, MINIMAL_VESTING_BALANCE );
  }

  /*********************************************************************/

  //                          PUBLIC FUNCTIONS

  /*********************************************************************/

  function vest( address multisig, address depool, bool for6months)
    public override
  {
    require( tvm.pubkey() == msg.pubkey(), EXN_AUTH_FAILED );
    require( g_amount > MIN_STAKE, EXN_BALANCE_TOO_LOW );
    require( !is_for6months.exists( depool ), EXN_DEPOOL_ALREADY_USED );
    tvm.accept();
    
    if( for6months ){
      uint64 amount = uint64( g_amount + g_bonus );

      IDePool( depool ).addVestingStake
        { flag: 1, value: amount + DEPOOL_FEE }
      ( amount, multisig, VESTING_INCREMENT, MAX_VESTING_PERIOD );
    } else {

      uint64 amount = uint64(
                              g_amount +
                              math.muldiv( g_bonus, MIN_VESTING_BONUS,
                                           MAX_VESTING_BONUS )
                              );
      IDePool( depool ).addVestingStake
        { flag: 1, value: amount + DEPOOL_FEE }
      ( amount, multisig, VESTING_INCREMENT, MIN_VESTING_PERIOD );
    }
    is_for6months[ depool ] = for6months ;
    g_amount = 0 ;
    g_bonus = 0 ;
  }

  function collect( address multisig ) public override
  {
    require( tvm.pubkey() == msg.pubkey(), EXN_AUTH_FAILED );
    require( now > math.min( g_deploy_date + MIN_VESTING_PERIOD,
                             AUTO_UNLOCK_DATE ),
             EXN_FUNDS_NOT_UNLOCKED );
    require( g_amount > 0, EXN_ALREADY_DEPLOYED );
    tvm.accept();
    
    if( now > math.min( g_deploy_date + MAX_VESTING_PERIOD,
                        AUTO_MAXBONUS_DATE ) ){
      uint128 amount = g_amount + g_bonus ;
      multisig.transfer({ value: amount, bounce: false, flag: 1});
    } else {
      uint128 amount = g_amount + math.muldiv( g_bonus, MIN_VESTING_BONUS,
                                               MAX_VESTING_BONUS );
      multisig.transfer({ value: amount, bounce: false, flag: 1});
    }
    g_amount = 0 ;
    g_bonus = 0 ;
  }

  function getCollectInfo() public override
    returns (uint64 collect_date1 ,
             uint64 collect_date2 ,
             uint128 amount,
             uint128 bonus
             )
  {
    collect_date1 =
      math.min( g_deploy_date + MIN_VESTING_PERIOD,
                AUTO_UNLOCK_DATE );
    collect_date2 =
      math.min( g_deploy_date + MAX_VESTING_PERIOD,
                AUTO_MAXBONUS_DATE ) ;
    amount = g_amount ;
    bonus = g_bonus ;
  }
  
  function closeSwap() public
  {
    require( now > MERGE_EXPIRATION_DATE, EXN_SWAP_NOT_EXPIRED );
    if( g_amount == 0 ){
      selfdestruct( address.makeAddrStd( 0, GIVER_MSIG_ADDR ) );
    }
  }

  /*********************************************************************/

  //                          PUBLIC CALLBACKS

  /*********************************************************************/

  function receiveAnswer(uint32 errcode,
                         uint64 // reason
                         ) public override
  {
    if( errcode != 0 ){
      _refill( msg.value, DEPOOL_FEE );
    }
  }

  /*********************************************************************/

  //                          PRIVATE FUNCTIONS

  /*********************************************************************/

  function _refill( uint128 added_amount,
                    uint128 minimal_vesting
                    ) internal
  {
    if( added_amount > minimal_vesting ) {
      uint8 bonus_part = MAX_VESTING_BONUS ;
      // if returned from depool, set correct bonus percentage :
      optional(bool) opt = is_for6months.fetch( msg.sender) ;
      if( opt.hasValue() ){
        if( !opt.get() ){ bonus_part = MIN_VESTING_BONUS; }
        delete is_for6months[ msg.sender ];
      }
      
      added_amount -= minimal_vesting ;
      uint128 bonus = math.muldiv( added_amount, bonus_part,
                                   NOVESTING_PART + bonus_part );
      g_bonus += bonus ;
      g_amount += added_amount - bonus ;
    }
  }

  /*********************************************************************/

  //                          SYSTEM

  /*********************************************************************/

  fallback () external pure
  {
    require( false, EXN_NO_FALLBACK );
  }
  
  receive () external 
  {
    _refill( msg.value, MINIMAL_VESTING_BALANCE );
  }

  onBounce(TvmSlice) external {
    _refill( msg.value, DEPOOL_FEE );
  }

}

