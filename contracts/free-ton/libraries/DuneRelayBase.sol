pragma ton-solidity >= 0.37.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

abstract contract DuneRelayBase {

  uint8 public g_nreqs ;         // required number of relays to confirm
  mapping(uint256 => uint8) public g_relays ; // pub_key -> relay index

  function _isRelay( uint256 key ) internal view
    returns ( uint8 index )
  {
    optional(uint8) opt_index = g_relays.fetch ( key );
    require ( opt_index.hasValue(), 100 ); // EXN_AUTH_FAILED is always 100
    index = opt_index.get() ;
  }

}
